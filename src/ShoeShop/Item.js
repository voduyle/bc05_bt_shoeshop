import React, { Component } from 'react'

export default class Item extends Component {
    render() {

        return (

            <div className="card col-3" >
                <img className="card-img-top" src={this.props.shoe.image} alt="Card image cap" />
                <div className="card-body">
                    <h5 className="card-title">{this.props.shoe.name}</h5>
                    <p className="card-text">${this.props.shoe.price}</p>
                    <button onClick={() => { this.props.handleDetail(this.props.shoe) }} className='btn btn-success mr-5'>Detail</button>
                    <button onClick={() => { this.props.handleAddToCart(this.props.shoe) }} className='btn btn-primary'>Cart</button>
                </div>
            </div>


        )
    }
}
