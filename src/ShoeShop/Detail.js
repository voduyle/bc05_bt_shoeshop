import React, { Component } from 'react'

export default class Detail extends Component {
    render() {
        return (
            <div className='row mt-5'>
                <div className='col-4'>
                    <img style={{ width: "100%" }} src={this.props.detail?.image} alt="" />
                </div>
                <div className='col-8'>
                    <h3>{this.props.detail?.name}</h3>
                    <p>{this.props.detail?.price}</p>
                    <p>{this.props.detail?.description}</p>
                </div>
            </div>
        )
    }
}
