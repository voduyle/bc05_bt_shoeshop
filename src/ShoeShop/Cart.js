import React, { Component } from 'react'

export default class Cart extends Component {
    render() {
        return (
            <table class="table">
                <thead>
                    <tr>
                        <th scope="col">Image</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">Quatity</th>
                        <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                    {this.props.cart.map((item) => {
                        return (
                            <tr>
                                <td><img style={{ width: "100px" }} src={item.image} alt="" /></td>
                                <td>{item.name}</td>
                                <td>${item.price * item.number}</td>
                                <td>
                                    <button className='btn btn-success mr-2' onClick={() => { this.props.handleGiamSoLuong(item) }}>-</button>
                                    {item.number}
                                    <button className='btn btn-warning ml-2' onClick={() => { this.props.handleTangSoLuong(item) }}>+</button>
                                </td>
                                <td><button className='btn btn-danger' onClick={() => { this.props.handleDeleteCart(item) }}>Delete</button></td>
                            </tr>
                        )
                    })
                    }

                </tbody>
            </table>
        )
    }
}
