import React, { Component } from 'react'
import Cart from './Cart'
import Detail from './Detail'
import Item from './Item'

export default class List extends Component {
  rendenShoeShop = () => {
    return this.props.data.map((item) => {
      return <Item shoe={item} handleDetail={this.props.handleDetail} handleAddToCart={this.props.handleAddToCart} />
    })
  }
  render() {
    return (
      <div>
        <Cart
          cart={this.props.cart}
          handleTangSoLuong={this.props.handleTangSoLuong} handleGiamSoLuong={this.props.handleGiamSoLuong}
          handleDeleteCart={this.props.handleDeleteCart} />
        <div className='row'>
          {this.rendenShoeShop()}
        </div>
        <Detail detail={this.props.detail} />
      </div>
    )
  }
}
